# gitlab_graphql/project.py
import requests
from . import session

from tests.gitlab_graphql import AUTH_TOKEN

class Project(object):
  def __init__(self, fullPath):
        self.fullPath = fullPath
  
  def info(self):
        headers = {"Authorization": "Bearer " + session.params['auth_token'], "Content-Type": "application/json"}

        queries = [] 
        queries.append("""
        query {
        project(fullPath: "sam-ultimate/example") {
        actualRepositorySizeLimit
        agentConfigurations {
            edges {
                node {
                agentName
                }
            }
            }
        allowMergeOnSkippedPipeline
        apiFuzzingCiConfiguration {
        scanModes
        scanProfiles {
            description
            name
            yaml
        }
        }
        archived
        autocloseReferencedIssues
        avatarUrl
        ciCdSettings {
        jobTokenScopeEnabled
        keepLatestArtifact
        mergeTrainsEnabled
        mergePipelinesEnabled
        }
        ciConfigPathOrDefault
        ciVariables {
        edges {
            cursor
            node {
            environmentScope
            id
            key
            masked
            protected
            raw
            value
            variableType
            }
        }
        nodes {
            environmentScope
            id
            key
            masked
            protected
            raw
            value
            variableType
        }
        pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
        }
        }
        codeCoverageSummary {
        averageCoverage
        coverageCount
        lastUpdatedOn
        }
        complianceFrameworks {
        edges {
            cursor
            node {
            color
            description
            id
            name
            pipelineConfigurationFullPath
            }
        }
        nodes {
            color
            description
            id
            name
            pipelineConfigurationFullPath
        }
        pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
        }
        }
        containerExpirationPolicy {
        cadence
        createdAt
        enabled
        keepN
        nameRegex
        nextRunAt
        nameRegexKeep
        olderThan
        updatedAt
        }
        containerRegistryEnabled
        containerRepositoriesCount
        corpuses {
        edges {
            cursor,
            node {
            id,
            }
        }
        nodes {
            id
        }
        pageInfo {
            endCursor
            hasNextPage
            hasPreviousPage
            startCursor
        }
        }
        createdAt
        dastScannerProfiles {
        edges {
            cursor
            node {
            editPath
            id
            profileName
            referencedInSecurityPolicies
            scanType
            showDebugMessages
            spiderTimeout
            targetTimeout
            useAjaxSpider
            }
        }
        nodes {
            editPath
            id
            profileName
            referencedInSecurityPolicies
            scanType
            showDebugMessages
            spiderTimeout
            targetTimeout
            useAjaxSpider
        }
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
        }
        dastSiteProfiles {
        edges {
            cursor
            node {
            auth {
                enabled
                password
                passwordField
                submitField
                url
                username
                usernameField
            }
            editPath
            excludedUrls
            id
            normalizedTargetUrl
            profileName
            referencedInSecurityPolicies
            requestHeaders
            scanMethod
            targetUrl
            userPermissions {
                createOnDemandDastScan
            }
            validationStatus
            }
        }
        nodes {
                auth {
                enabled
                password
                passwordField
                submitField
                url
                username
                usernameField
            }
            editPath
            excludedUrls
            id
            normalizedTargetUrl
            profileName
            referencedInSecurityPolicies
            requestHeaders
            scanMethod
            targetUrl
            userPermissions {
                createOnDemandDastScan
            }
            validationStatus
            }
        pageInfo {
            hasNextPage
            hasPreviousPage
            startCursor
            endCursor
        }
        }
        }
        }
        """)

        queries.append("""
        query {
        project(fullPath: "sam-ultimate/example") {
        description
        descriptionHtml
        forksCount
        fullPath
        grafanaIntegration {
        createdAt
        enabled
        grafanaUrl
        id
        updatedAt
        }
        group {
        actualRepositorySizeLimit
        additionalPurchasedStorageSize
        allowStaleRunnerPruning
        autoDevopsEnabled
        avatarUrl
        ciVariables {
                edges {
                    cursor
                    node {
                    environmentScope
                    id
                    key
                    masked
                    protected
                    raw
                    value
                    variableType
                    }
                }
                nodes {
                    environmentScope
                    id
                    key
                    masked
                    protected
                    raw
                    value
                    variableType
                }
                pageInfo {
                    endCursor
                    hasNextPage
                    hasPreviousPage
                    startCursor
                }
                }
        containerRepositoriesCount
        containsLockedProjects
        crossProjectPipelineAvailable
        }
        httpUrlToRepo
        id
        importStatus
        issuesEnabled
        jiraImportStatus
        jiraImports {
        edges {
            cursor
            node {
            createdAt
            failedToImportCount
            jiraProjectKey
            scheduledAt
            scheduledBy {
                id
            }
            totalIssueCount
            }
        }
        nodes {
            createdAt
            failedToImportCount
            jiraProjectKey
            scheduledAt
            scheduledBy {
                id
            }
            totalIssueCount
        }
        pageInfo {
            endCursor
            startCursor
        }
        }
        jobsEnabled
        lastActivityAt
        lfsEnabled
        mergeCommitTemplate
        mergeRequestsEnabled
        mergeRequestsFfOnlyEnabled
        name
        nameWithNamespace
        namespace {
        actualRepositorySizeLimit
            additionalPurchasedStorageSize
            containsLockedProjects
            crossProjectPipelineAvailable
            description
            descriptionHtml
            fullName
            fullPath
            id
            isTemporaryStorageIncreaseEnabled
            lfsEnabled
            name
            packageSettings {
            genericDuplicatesAllowed
            genericDuplicateExceptionRegex
            mavenDuplicatesAllowed
            mavenDuplicateExceptionRegex
        }
            path
            repositorySizeExcessProjectCount
            requestAccessEnabled
            rootStorageStatistics {
            buildArtifactsSize
                containerRegistrySize
                dependencyProxySize
                lfsObjectsSize
                packagesSize
                pipelineArtifactsSize
                repositorySize
                snippetsSize
                storageSize
                uploadsSize
                wikiSize
        }
            sharedRunnersSetting
            storageSizeLimit
            temporaryStorageIncreaseEndsOn
            timelogCategories {
            edges {
            cursor
            node {
                billable
                        billingRate
                        color
                        createdAt
                        description
                        id
                        name
                        updatedAt
            }
            }
            nodes {
            billable
                        billingRate
                        color
                        createdAt
                        description
                        id
                        name
                        updatedAt
            }
            pageInfo {
            startCursor
            endCursor
            hasNextPage
            hasPreviousPage
            }
        }
            totalRepositorySize
            totalRepositorySizeExcess
            visibility
        }
        onlyAllowMergeIfAllDiscussionsAreResolved
        onlyAllowMergeIfPipelineSucceeds
        openIssuesCount
        packagesCleanupPolicy {
        keepNDuplicatedPackageFiles
        nextRunAt
        }
        }
        }
        """)

        full_query_response = {}
        # Query has complexity of 250, so we have to query in batches of 250
        for query in queries:
            response = requests.post("https://gitlab.com/api/graphql", json={"query":query}, headers=headers)
            data = response.json()
            query_subset = data['data']['project']
            full_query_response = {**full_query_response, **query_subset}
        
        return full_query_response

