# gitlab-graphql/__init__.py

import os
import requests

AUTH_TOKEN = os.environ.get('AUTH_TOKEN', None)

class AuthToken(Exception):
    pass

if AUTH_TOKEN is None:
    raise AuthToken(
        """All methods require an auth token.
        You can set the token by typing export AUTH_TOKEN="{YOUR_TOKEN}" in your terminal."""
    )
session = requests.Session()
session.params = {}
session.params['auth_token'] = AUTH_TOKEN

from .project import Project