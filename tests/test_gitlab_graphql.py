# tests/test_gitlab_graphql.py

from pytest import fixture
from gitlab_graphql import Project

@fixture
def project_keys():
    # Place all project keys here
    return ['createdAt']
    '''
    return ['actualRepositorySizeLimit','agentConfigurations','allowMergeOnSkippedPipeline','apiFuzzingCiConfiguration','archived','autocloseReferencedIssues',
    'avatarUrl','ciCdSettings','ciConfigPathOrDefault','ciJobTokenScope','ciVariables','codeCoverageSummary','complianceFrameworks','containerExpirationPolicy',
    'containerRegistryEnabled','containerRepositoriesCount','corpuses','createdAt','dastScannerProfiles','dastSiteProfiles','description','descriptionHtml','dora',
    'forksCount','fullPath','grafanaIntegration','group','httpUrlToRepo','id','importStatus','issuesEnabled','jiraImportStatus','jiraImports','jobsEnabled','lastActivityAt',
    'lfsEnabled','mergeCommitTemplate','mergeRequestsEnabled','mergeRequestsFfOnlyEnabled','name','nameWithNamespace','namespace','onlyAllowMergeIfAllDiscussionsAreResolved',
    'onlyAllowMergeIfPipelineSucceeds','openIssuesCount','packagesCleanupPolicy','path','pathLocks','pipelineAnalytics','printingMergeRequestLinkEnabled','publicJobs','pushRules',
    'recentIssueBoards','removeSourceBranchAfterMerge','repository','repositorySizeExcess','requestAccessEnabled','requirementStatesCount','sastCiConfiguration','scanResultPolicies',
    'securityDashboardPath','securityScanners','sentryErrors','serviceDeskAddress','serviceDeskEnabled','sharedRunnersEnabled','snippetsEnabled','squashCommitTemplate','squashReadOnly',
    'sshUrlToRepo','starCount','statistics','suggestionCommitMessage','tagList','terraformStates','timelogCategories','topics','userPermissions','visibility','vulnerabilityImages','vulnerabilityScanners',
    'webUrl','wikiEnabled']
    '''

def test_project_info(project_keys):
    """Tests an API call to get a Project info"""

    project_instance = Project('sam-ultimate/example')
    response = project_instance.info()

    assert isinstance(response, dict)
    assert response['createdAt'] == '2022-08-09T19:47:57Z', "The createdAt should be in the response"
    assert set(project_keys).issubset(response.keys()), "All keys should be in the response"